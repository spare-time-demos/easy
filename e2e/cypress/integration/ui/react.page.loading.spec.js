/// <reference types="cypress" />

context('react page is loading', () => {
  
  beforeEach(() => {
    cy.visit('index.html')
  })

  it('generic react page shown on root', () => {
    cy.get('body').contains('and save to reload').should('exist')
  })

})