/// <reference types="cypress" />

context('server rendered hello page is loading', () => {
  
  beforeEach(() => {
    cy.visit('hello')
  })

  it('hello', () => {
    cy.get('body').contains('hello').should('exist')
  })

})