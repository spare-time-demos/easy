= development environment


== install a development environment

[source,shell]
----
choco upgrade gitextensionschoco 
choco upgrade intellijidea-community
----

* docker
* intellij plugin: asciidoc

== project layout

[source,shell]
----
/docs   documentation
/server spring boot server project
----

== development workflow

* gitlab: create issue
* gitlab: create merge request for issue
* git: checkout merge request branch locally

* develop

** init database
*** `docker run --rm --name postgres -e POSTGRES_PASSWORD=postgres -d -p 5432:5432 postgres`
*** `./gradlew dropall update`

** develop server
*** cd client
*** run server test: `./gradle test`
*** run server:  ./gradle bootRun

** develop client
*** cd client
*** npm install
*** run client: npm start
*** build client: npm test

** run e2e test
*** cd e2e
*** npm install
*** npm run open
*** npm run test:ci

* git: commit 
** tbd: commit comment

* git: push to branch
* gitlab: resolve merge request

== some other commands:

* run e2e test setup locally:  docker-compose -f docker-compose.e2e-test.yml up --exit-code-from=e2etests --abort-on-container-exit

== endpoints

* http://localhost:8080/actuator/prometheus


== further reading

* https://spring.io/guides/gs/spring-boot-docker/
* 