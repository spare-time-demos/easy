package org.manathome.easy;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/** dummy unit test, checking test runner only. */
public class SimpleUnitTest {

  @Test
  void trivialOk() {
    Assertions.assertTrue(true, "will obviously never fail");
  }

}
