package org.manathome.easy;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

/** test application startup without fault. */
@SpringBootTest
public class EasyApplicationTests {

  @Test
  public void contextSpringBootServerLoads() {
  }

}
