package org.manathome.easy.api;

import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

/**
 * test rest call.
 */
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class TeamMemberResourceTest {

  @Autowired
  public MockMvc mvc;

  @Test
  public void testGetTeamMembers() throws Exception {

    mvc.perform(get(TeamMemberResource.PATH_TEAMMEMBER))
        .andDo(print())
        .andExpect(status()
        .isOk())
        .andExpect(jsonPath("$[0].id", org.hamcrest.Matchers.is(-1)))
        .andExpect(jsonPath("$[0].name", org.hamcrest.Matchers.is("a first test lead and user")))
        .andDo(document(TeamMemberResource.PATH_TEAMMEMBER + "/GET"));
  }

}
