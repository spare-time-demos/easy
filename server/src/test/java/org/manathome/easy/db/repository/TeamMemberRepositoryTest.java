package org.manathome.easy.db.repository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;
import org.manathome.easy.domain.TeamMember;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * test db access team member.
 */
@DataJpaTest
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class TeamMemberRepositoryTest {

  @Autowired
  TeamMemberRepository repository;

  @Test
  public void storeNewMemberIntoDatabase() {
    assertThat(repository).isNotNull();

    var t = new TeamMember("tUser", "test user");
    var savedT = repository.save(t);
    assertThat(savedT).isNotNull();
    assertThat(savedT.getId()).isGreaterThan(0);
  }

  @Test
  public void findAtLeastOneMemberInDatabase() {

    var foundMembers = StreamSupport
        .stream(repository.findAll().spliterator(), false)
        .collect(Collectors.toList());

    assertThat(foundMembers).isNotNull();
    assertThat(foundMembers.size()).isGreaterThan(0);
  }
}

