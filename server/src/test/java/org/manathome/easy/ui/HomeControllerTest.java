package org.manathome.easy.ui;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.core.StringContains.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * test root html page.
 */
@SpringBootTest
@AutoConfigureMockMvc
@AutoConfigureRestDocs
public class HomeControllerTest {

  @Autowired
  public MockMvc mvc;

  @Test
  public void testHomePage() throws Exception {

    mvc.perform(get("/"))
        .andExpect(status()
        .isOk())
        .andExpect(content().string(containsString("<h1>easy")))
    ;
  }

}
