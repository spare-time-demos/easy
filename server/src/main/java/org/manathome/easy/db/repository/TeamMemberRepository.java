package org.manathome.easy.db.repository;

import org.manathome.easy.domain.TeamMember;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.Optional;

/** db access. */
public interface TeamMemberRepository extends PagingAndSortingRepository<TeamMember, Long> {

  Optional<TeamMember> findByUserId(String userId);

}

