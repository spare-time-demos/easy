package org.manathome.easy.api;

import io.swagger.v3.oas.annotations.Operation;
import org.manathome.easy.db.repository.TeamMemberRepository;
import org.manathome.easy.domain.TeamMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/** rest api for team member. */
@RestController
@CrossOrigin
public class TeamMemberResource {

  private static final Logger log = LoggerFactory.getLogger(TeamMemberResource.class);

  public static final String PATH_TEAMMEMBER = ResourceHelper.PATH_BASE_REST + "teammember";

  @Autowired
  private TeamMemberRepository tmRepository;

  /** get /teammember. */
  @GetMapping(path = PATH_TEAMMEMBER, produces = ResourceHelper.CONTENT_TYPE_JSON)
  @Operation(summary = "get all teammembers", operationId = "getAllTeamMembers")
  public List<TeamMember> list() {
    log.trace("GET getAllTeamMembers");
    return StreamSupport
      .stream(tmRepository.findAll().spliterator(), false)
      .collect(Collectors.toList());
  }
}
