package org.manathome.easy.api;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/** rest api sample. */
@RestController
@CrossOrigin
public class ExampleResource {

  @GetMapping(path = "hello")
  @Operation(summary = "dummy endpoint, returns hello")
  @ApiResponse(responseCode = "200", description = "static text hello", content = @Content(mediaType = "text/hml"))
  public String hello() {
    return "hello";
  }
}
