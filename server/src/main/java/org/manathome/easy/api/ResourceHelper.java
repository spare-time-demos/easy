package org.manathome.easy.api;

/** some constants. */
public final class ResourceHelper {

  public static final String PATH_BASE_REST = "/rest/";

  public static final String CONTENT_TYPE_JSON = "application/json";

  private ResourceHelper() {

  }

}
