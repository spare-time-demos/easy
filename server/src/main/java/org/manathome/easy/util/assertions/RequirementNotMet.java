package org.manathome.easy.util.assertions;

import org.manathome.easy.util.Logger;

/** assert runtime exception. */
public class RequirementNotMet extends RuntimeException {

  private static final Logger logger = Logger.getLogger(AssertionError.class);
  
  private static final long serialVersionUID = 1L;
  
  public RequirementNotMet(final String msg) {
    super(msg);
    logger.debug("RequirementNotMet({})", msg);
  }

}
