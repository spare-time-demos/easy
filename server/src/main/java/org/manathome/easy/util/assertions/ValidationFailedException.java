package org.manathome.easy.util.assertions;

import org.manathome.easy.util.Logger;

/** check runtime exception. */
public class ValidationFailedException extends RequirementNotMet {

  private static final Logger logger = Logger.getLogger(ValidationFailedException.class);
  
  private static final long serialVersionUID = 1L;
  
  public ValidationFailedException(final String msg) {
    super(msg);
    logger.debug("ValidationFailedException({})", msg);
  }
}
