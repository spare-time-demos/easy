package org.manathome.easy.util;

import org.slf4j.LoggerFactory;
import org.slf4j.spi.LocationAwareLogger;

/** small logging wrapper wrapper (sl4j), do not do this. */
public final class Logger {

  private static final String FQCN = Logger.class.getName();
  
  private final LocationAwareLogger loggerImplementation;
  
  private Logger(final LocationAwareLogger logger) {
    this.loggerImplementation = logger;
  }
  
  /** builder. */
  public static Logger getLogger(final Class<?> c) {

    org.slf4j.Logger l = LoggerFactory.getLogger(c);
    if (l instanceof LocationAwareLogger) {
      LocationAwareLogger lal = (LocationAwareLogger) l;
      return new Logger(lal);      
    }
    throw new EasyInternalException("location aware logger could not be created.");
  }
  
  /** builder. */
  public static Logger getLogger(final String name) {

    final org.slf4j.Logger l = LoggerFactory.getLogger(name);
    if (l instanceof LocationAwareLogger) {
      LocationAwareLogger lal = (LocationAwareLogger) l;
      return new Logger(lal);      
    }
    throw new EasyInternalException("location aware logger could not be created for " + name);
  }

  public void error(final String msg, Object... args) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, args, null);
  }

  public void error(final String msg, Throwable t, Object... args) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, args, t);
  }

  public void fatal(final String msg, Throwable t) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, null, t);
  }

  public void info(final String msg) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.INFO_INT, msg, null, null);
  }

  public void trace(final String msg) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.TRACE_INT, msg, null, null);
  }

  public void debug(final String msg) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, null, null);
  }

  public void debug(final String msg,  Object... args) {
    this.loggerImplementation.log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, args, null);
  }

}
