package org.manathome.easy.util;

/** internal coding/unexpected technical error. */
public class EasyInternalException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EasyInternalException() {
    super();
  }

  public EasyInternalException(final String message) {
    super(message);
  }

  public EasyInternalException(final String message, final Throwable cause) {
    super(message, cause);
  }

}
