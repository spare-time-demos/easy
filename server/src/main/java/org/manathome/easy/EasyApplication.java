package org.manathome.easy;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/** easy server. */
@SpringBootApplication
public class EasyApplication {

  /**
   * main entry point, starts easy server.
   */
  public static void main(String[] args) {
    SpringApplication.run(EasyApplication.class, args);
  }

}
