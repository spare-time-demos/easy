package org.manathome.easy.batch;

import org.manathome.easy.db.repository.TeamMemberRepository;
import org.manathome.easy.domain.TeamMember;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.data.RepositoryItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Sort;

import java.util.HashMap;
import java.util.Map;

/** dummy batch configuration. */
@Configuration
@EnableBatchProcessing
public class BatchConfiguration  {

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  public StepBuilderFactory stepBuilderFactory;

  @Autowired
  public TeamMemberRepository teamMemberRepository;

  /** dummy batch part. */
  @Bean
  public TeamMemberProcessor stepTeamMemberProcessor() {
    return new TeamMemberProcessor();
  }

  /** dummy batch part. */
  @Bean
  public TeamMemberProcessor stepTeamMemberNopWriter() {
    return new TeamMemberProcessor();
  }

  /** dummy batch part. */
  @Bean
  public TeamMemberNoopWriter stepTeamMemberWriter() {
    return new TeamMemberNoopWriter();
  }

  /** dummy batch part. */
  @Bean
  public RepositoryItemReader<TeamMember> stepTeamMemberReader() {

    Map<String, Sort.Direction> sortMap = new HashMap<>();
    sortMap.put("id", Sort.Direction.ASC);

    RepositoryItemReader<TeamMember> repositoryItemReader = new RepositoryItemReader<>();
    repositoryItemReader.setRepository(teamMemberRepository);
    repositoryItemReader.setPageSize(5);
    repositoryItemReader.setSort(sortMap);
    repositoryItemReader.setMethodName("findAll");
    return repositoryItemReader;
  }

  /** dummy batch part. */
  @Bean
  public Job jobMonitorMembers(Step step1) {
    return jobBuilderFactory.get("jobMonitorMembers")
      .incrementer(new RunIdIncrementer())
      .flow(step1)
      .end()
      .build();
  }

  /** dummy batch part. */
  @Bean
  public Step step1() {
    return stepBuilderFactory.get("stepProcessMembers")
      .<TeamMember, TeamMember>chunk(10)
      .reader(stepTeamMemberReader())
      .processor(stepTeamMemberProcessor())
      .writer(stepTeamMemberWriter())
      .build();
  }
}
