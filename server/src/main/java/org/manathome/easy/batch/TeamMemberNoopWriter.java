package org.manathome.easy.batch;

import org.manathome.easy.domain.TeamMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemWriter;

import java.util.List;

/** dummy batch part. */
public class TeamMemberNoopWriter implements ItemWriter<TeamMember> {

  private static final Logger log = LoggerFactory.getLogger(TeamMemberNoopWriter.class);


  @Override
  public void write(List<? extends TeamMember> items) throws Exception {
    log.info("noop writer write({})", items.size());
  }
}
