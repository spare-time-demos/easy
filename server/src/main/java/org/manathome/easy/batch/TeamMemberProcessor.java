package org.manathome.easy.batch;

import org.manathome.easy.domain.TeamMember;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

/** dummy batch part. */
public class TeamMemberProcessor implements ItemProcessor<TeamMember, TeamMember> {

  private static final Logger log = LoggerFactory.getLogger(TeamMemberProcessor.class);

  @Override
  public TeamMember process(TeamMember tm) throws Exception {
    log.trace("process({})", tm.getUserId());
    return tm;
  }
}
