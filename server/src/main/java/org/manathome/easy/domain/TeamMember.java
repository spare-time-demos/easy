package org.manathome.easy.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import io.swagger.v3.oas.annotations.media.Schema;
import org.manathome.easy.util.assertions.Require;

/** employee or worker that leads or works on a project. */
@Entity
@Table(name = TeamMember.TABLE_NAME)
@Schema(description = "a teammember (user), may work on tasks or work as admin or project lead.")
public class TeamMember implements Serializable {
  
  /**
   * required for caching.
   */
  private static final long serialVersionUID = -3338312632276995655L;

  public static final String TABLE_NAME = "HX_TEAM_MEMBER";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Long id;

  @Column(name = "name", length = 100, nullable = false, unique = true)
  private String name;

  @Column(name = "user_id", length = 25, nullable = false, unique = true)
  private String userId;
  
  @Column(name = "password", length = 25, nullable = true)
  @Schema(hidden = true)
  @JsonIgnore
  private String password;
  
  @Column(name = "is_locked", nullable = false)
  @Schema(hidden = true)
  private int lockedFlag;

  @Column(name = "is_active", nullable = false)
  @Schema(hidden = true)
  private int activeFlag;

  @Column(name = "is_admin", nullable = false)
  @Schema(hidden = true)
  private int adminRoleFlag;

  @Column(name = "is_lead", nullable = false)
  @Schema(hidden = true)
  private int leadRoleFlag;
  
  
  /** builder. */
  public static TeamMember buildForTest(long id, String name, String userId) {
    return new TeamMember(Long.valueOf(id), name, userId);
  }

  /** jpa required ctor. */
  protected TeamMember() {
  }
  
  /** ctor. */
  public TeamMember(final Long id, final String userId, final String name) {
    this(userId, name);
    this.id = id;
  }

  /** ctor. */
  public TeamMember(final String userId, final String name) {
    this.name = Require.notNull(name, "name");
    this.userId = Require.notNull(userId, "userId");
  }
  
  
  

  @Schema(description = "unique id", example = "1234")
  public Long getId() {
    return id;
  }

  @Schema(description = "user id (used by login, unique", required = true,  example = "tlead1", maxLength = 25)
  public String getUserId() {
    return userId;
  }
    
  public void setUserId(final String userId) {
    this.userId = userId;
  }

  @Schema(description = "user name", required = true, example = "max musterman", maxLength = 100)
  public String getName() {
    return name;
  }

  public void setName(final String name) {
    this.name = Require.notNull(name);
  }
  
  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }  


  @Schema(description = "is account locked (no login possible)", example = "false")  
  public boolean isLocked() {
    return lockedFlag == 1;
  }

  public void setLocked(boolean locked) {
    this.lockedFlag = locked ? 1 : 0;
  }

  @Schema(description = "is user active (may be used on new tasks)", example = "false")
  public boolean isActive() {
    return activeFlag == 1;
  }

  public void setActive(boolean active) {
    this.activeFlag = active ? 1 : 0;
  }

  @Schema(description = "is user a application admin", example = "false")
  public boolean isAdminRole() {
    return adminRoleFlag == 1;
  }

  public void setAdminRole(boolean isAdmin) {
    this.adminRoleFlag = isAdmin ? 1 : 0;
  }

  @Schema(description = "is user a project lead (may add tasks in a project)", example = "false")
  public boolean isLeadRole() {
    return leadRoleFlag == 1;
  }

  public void setLeadRole(boolean isLead) {
    this.leadRoleFlag = isLead ? 1 : 0;
  }  
  
  @Override
  public String toString() {
    return "TM[" + id + ": " + userId + ", " + name + "]";
  }

  /** current team member may log in into app. */
  public boolean userMayLogIn() {
    return isActive() 
        && ! isLocked() 
        && this.getUserId() != null 
        && this.getPassword() != null;
  }

}
