package org.manathome.easy.ui;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Date;

/** simple home controller, server side thymeleaf "home" template rendered. */
@Controller
public class HomeController {

  @GetMapping("/")
  public String home(Model model) {
    model.addAttribute("today", (new Date()).toString());
    return "home";
  }

}
